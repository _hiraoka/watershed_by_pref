# 都道府県水系占有率調査 
## 滋賀県の水はすべて淀川に流れているのか

ある映画を見て、滋賀県の水がどの程度淀川に流れるのか知らないことに気づきました。少し検索しても具体的な数値が出てこなかったので、計算してみることにしました。ついでに全都道府県調べてみました。

## 元データ

* 国土数値情報 行政区域データ(2021年)
* 国土数値情報 流域メッシュ(2009年)

## 結果
### 滋賀県と淀川水系
滋賀県の水がすべて淀川に流れているわけではなく、北川や木曽川にも流れています。しかし、県のほとんどの面積、98.68%が淀川水系です。

<img src="docs/shiga_watersheds.svg" width="50%" style="margin-bottom: 1em">

![Alt text](docs/shiga_coverage.png)

滋賀県の北川水系部分を見ると、県境部分の深い谷がなければ、水坂峠を水が流れ、この部分も淀川水系になりうると感じます。ひょっとすると、現在の県境は大昔の分水嶺だったのではないかと思います。

![Alt text](docs/shiga_kitakawa.png)

木曽川水系部分を見ると、北川と同様に県境部分の谷がなければ、この部分は淀川水系である可能性が高いです。この谷は関ヶ原断層上にあります。

![Alt text](docs/shiga_kisogawa.png)

滋賀県の淀川水系以外の部分は大昔は淀川水系であった可能性が十分にあります。

### 滋賀県-淀川を超える水系占有率を持つ組み合わせ 群馬県-利根川

同じデータを用いて、すべての都道府県について水系の占有率を調査しました。山形県-最上川ペアが善戦すると予想していた一方、滋賀県を上回るものはないと高をくくっていました。が、トップに立ったのは群馬県-利根川ペアでした。利根川水系の群馬占有率は98.72%でした。

<img src="docs/gunma_watersheds.svg" width="50%" style="margin-bottom: 1em">

![Alt text](docs/gunma_coverage.png)

群馬県の阿賀野川水系部分、信濃川水系部分を確認しましたが、滋賀県のように谷が埋まれば逆に流れそうな箇所はありませんでした。

![Alt text](docs/gunma_aganogawa.png)

### 水系占有率都道府県別トップ3

全都道府県別の水系占有率トップ3は以下の通りです。

![水系占有率](./docs/watershed_by_pref.svg)

| # | 都道府県 | 占有1位 | % | 占有2位 | % | 占有3位 | % |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 01 | 北海道 | 石狩川 | 17.17 | 十勝川 | 10.81 | 天塩川 | 6.7 |
| 02 | 青森県 | 岩木川 | 25.52 | 高瀬川 | 8.85 | 奥入瀬川 | 7.11 |
| 03 | 岩手県 | 北上川 | 51.75 | 馬淵川 | 8.9 | 閉伊川 | 4.87 |
| 04 | 宮城県 | 北上川 | 32.4 | 阿武隈川 | 18.47 | 鳴瀬川 | 17.85 |
| 05 | 秋田県 | 雄物川 | 40.69 | 米代川 | 32.95 | 子吉川 | 10.15 |
| 06 | 山形県 | 最上川 | 73.31 | 赤川 | 9.21 | 荒川 | 8 |
| 07 | 福島県 | 阿賀野川 | 41.61 | 阿武隈川 | 29.01 | 夏井川 | 4.59 |
| 08 | 茨城県 | 利根川 | 54.12 | 那珂川 | 17.61 | 久慈川 | 15.41 |
| 09 | 栃木県 | 利根川 | 65.66 | 那珂川 | 33.8 | 久慈川 | 0.45 |
| 10 | 群馬県 | 利根川 | 98.72 | 阿賀野川 | 1 | 信濃川 | 0.28 |
| 11 | 埼玉県 | 荒川 | 65.05 | 利根川 | 34.92 | 多摩川 | 0.01 |
| 12 | 千葉県 | 利根川 | 31.75 | 夷隅川 | 5.81 | 小櫃川 | 5.43 |
| 13 | 東京都 | 多摩川 | 43.85 | 荒川 | 21.23 | 利根川 | 4.55 |
| 14 | 神奈川県 | 相模川 | 25.54 | 酒匂川 | 16.05 | 境川 | 8.45 |
| 15 | 新潟県 | 信濃川 | 37.89 | 阿賀野川 | 15.37 | 関川 | 8.66 |
| 16 | 富山県 | 神通川 | 17.63 | 黒部川 | 16.7 | 小矢部川 | 15.89 |
| 17 | 石川県 | 手取川 | 19.23 | 大野川 | 7.67 | 梯川 | 6.85 |
| 18 | 福井県 | 九頭竜川 | 67.5 | 北川 | 4.12 | 笙の川 | 3.63 |
| 19 | 山梨県 | 富士川 | 69.56 | 相模川 | 23.62 | 多摩川 | 4.64 |
| 20 | 長野県 | 信濃川 | 54.68 | 天竜川 | 27.36 | 木曽川 | 11.41 |
| 21 | 岐阜県 | 木曽川 | 67.13 | 神通川 | 18.76 | 庄川 | 6.89 |
| 22 | 静岡県 | 大井川 | 16.43 | 天竜川 | 13.74 | 狩野川 | 11.2 |
| 23 | 愛知県 | 矢作川 | 28.72 | 豊川 | 14.28 | 庄内川 | 13.02 |
| 24 | 三重県 | 宮川 | 15.97 | 淀川 | 12.3 | 雲出川 | 9.2 |
| 25 | 滋賀県 | 淀川 | 98.68 | 北川 | 1.02 | 木曽川 | 0.25 |
| 26 | 京都府 | 淀川 | 40.24 | 由良川 | 36.18 | 竹野川 | 4.49 |
| 27 | 大阪府 | 淀川 | 50.26 | 大和川 | 18.75 | 大津川 | 5.67 |
| 28 | 兵庫県 | 加古川 | 21.35 | 円山川 | 15.57 | 揖保川 | 9.62 |
| 29 | 奈良県 | 新宮川 | 40.28 | 紀の川 | 22.61 | 大和川 | 19.51 |
| 30 | 和歌山県 | 紀の川 | 19.52 | 日高川 | 13.72 | 新宮川 | 10.85 |
| 31 | 鳥取県 | 千代川 | 33.9 | 日野川 | 24.8 | 天神川 | 13.87 |
| 32 | 島根県 | 斐伊川 | 36.71 | 江の川 | 18.78 | 高津川 | 16.27 |
| 33 | 岡山県 | 吉井川 | 29.7 | 高梁川 | 28.21 | 旭川 | 25.82 |
| 34 | 広島県 | 江の川 | 31.04 | 太田川 | 19.99 | 芦田川 | 9.92 |
| 35 | 山口県 | 錦川 | 14.52 | 阿武川 | 11.2 | 佐波川 | 7.59 |
| 36 | 徳島県 | 吉野川 | 55.64 | 那賀川 | 21.44 | 勝浦川 | 5.51 |
| 37 | 香川県 | 綾川 | 8.05 | 財田川 | 8.04 | 新川 | 7.48 |
| 38 | 愛媛県 | 肱川 | 21.35 | 仁淀川 | 11.22 | 重信川 | 8.12 |
| 39 | 高知県 | 渡川 | 25.09 | 吉野川 | 14.77 | 仁淀川 | 13.2 |
| 40 | 福岡県 | 遠賀川 | 21.03 | 筑後川 | 20.67 | 矢部川 | 12.24 |
| 41 | 佐賀県 | 松浦川 | 18 | 筑後川 | 16.43 | 嘉瀬川 | 15.2 |
| 42 | 長崎県 | 佐々川 | 2.02 | 本明川 | 1.97 | 川棚川 | 1.95 |
| 43 | 熊本県 | 球磨川 | 25.29 | 緑川 | 14.69 | 菊池川 | 13.37 |
| 44 | 大分県 | 大野川 | 19.2 | 筑後川 | 17.71 | 大分川 | 10.43 |
| 45 | 宮崎県 | 大淀川 | 24.89 | 五ヶ瀬川 | 18.13 | 耳川 | 10.3 |
| 46 | 鹿児島県 | 川内川 | 14.11 | 肝属川 | 5.62 | 天降川 | 4.45 |
| 47 | 沖縄県 | 浦内川 | 2.88 | 比謝川 | 1.86 | 安波川 | 1.85 |

[占有率1%以上の河川一覧](./docs/river_rank_detail.md)

## 留意事項
### 水系データはメッシュ形式

水系データは100mメッシュ単位であるため精度は1ha程度です。

### 水系データには所属水系が不明な箇所がある

流域界のみ設定されているが、河川名、水系が設定されていない領域があります。
東日本に多く、他の水系に包含されている場合には不明領域を包含している領域に結合して処理しています。他水系に包含されていない領域はそのままにして、その他水系扱いとしています。

![Alt text](./docs/fukushima_unknown.png)

## プログラムの使い方
### 必要なもの
* bash
* PostgreSQL/PostGIS

### 使い方
#### データベース設定

config.shを編集し、PostgreSQLへの接続情報を設定する。

#### 処理実行
作成シェルを実行する
  ```code bash
  bash create_sheds.sh
  ```
待つこと1時間ぐらいでできあがり(WSL環境)。

#### 水系占有率グラフをみる  
[chart/chart.html](./chart/chart.html)

#### 都道府県別水系データを見る  
* watershed_by_prefテーブル  
  都道府県別水系データを格納

* watershed_ratioテーブル  
  水系占有率とランキングを格納

## 処理内容
実際の処理を行うSQLは以下の通りです。

[sql/create_watershed_by_pref.sql](./sql/create_watershed_by_pref.sql)

```code sql
-- 河川ごとに流域メッシュを集計
DROP TABLE IF EXISTS watershed;
CREATE TABLE watershed
(
    gid        SERIAL PRIMARY KEY,
    river_code VARCHAR,
    name       VARCHAR,
    geom       geometry
);

INSERT INTO watershed (river_code, name, geom)
SELECT w07_002                              AS river_code,
       w07_004                              AS name,
       ST_Union(st_buffer(geom, 0.0000001)) AS geom
FROM watershed_mesh
WHERE w07_002 IS NOT NULL
  AND w07_004 IS NOT NULL
  -- 結構エイヤなデータがあるので除外
  AND w07_004 <> 'unknown'
  AND w07_004 <> '名称不明'
  AND w07_004 <> '不明'
  AND w07_004 <> '川'
  AND w07_004 <> '明'
GROUP BY river_code, name
;
CREATE INDEX idx_watershed_geom
    ON watershed USING gist (geom);

-- 流域メッシュの外周だけを抽出
DROP TABLE IF EXISTS watershed_boundary;
CREATE TABLE watershed_boundary
(
    gid        SERIAL PRIMARY KEY,
    river_code VARCHAR,
    name       VARCHAR,
    geom       geometry
)
;
INSERT INTO watershed_boundary (river_code, name, geom)
SELECT river_code,
       name,
       st_makepolygon(st_exteriorring(st_geometryn(st_multi(geom), 1))) AS geom
FROM watershed;

CREATE INDEX idx_watershed_boundary_geom
    ON watershed_boundary USING gist (geom);

-- 他の流域に含まれる流域を削除
DELETE
FROM watershed_boundary a
    USING watershed_boundary b
WHERE ST_Contains(b.geom, a.geom)
  AND a.gid <> b.gid;

-- 行政界ポリゴンにインデックス
CREATE INDEX idx_town_geom
    ON town USING gist (geom);

-- 都道府県ポリゴン
DROP TABLE IF EXISTS pref;
CREATE TABLE pref
(
    gid  SERIAL PRIMARY KEY,
    id   VARCHAR,
    name VARCHAR,
    area DOUBLE PRECISION,
    geom geometry
);

-- 都道府県ポリゴンを作成
INSERT INTO pref(name, geom)
SELECT n03_001, st_union(st_buffer(geom, 0.0000001)) AS geom
FROM town
GROUP BY n03_001;
CREATE INDEX idx_pref_geom
    ON pref USING gist (geom);

-- 都道府県ポリゴンの面積を計算
UPDATE pref
SET area = st_area(st_geogfromwkb(geom), TRUE);

-- 都道府県コードを設定
UPDATE pref p
SET id = (SELECT SUBSTR(n03_007, 1, 2)
          FROM town g
          WHERE p.name = g.n03_001
          LIMIT 1);


-- 都道府県ごとに流域を分割
DROP TABLE IF EXISTS watershed_by_pref;
CREATE TABLE watershed_by_pref
(
    river_code VARCHAR,
    river      VARCHAR,
    pref       VARCHAR,
    area       DOUBLE PRECISION,
    geom       geometry
);
INSERT INTO watershed_by_pref (river_code, river, pref, geom)
SELECT a.river_code,
       a.name                          AS river,
       b.name                          AS pref,
       st_intersection(a.geom, b.geom) AS geom
FROM watershed_boundary a
         JOIN pref b
              ON st_intersects(a.geom, b.geom)
;
CREATE INDEX idx_watershed_by_pref_geom
    ON watershed_by_pref USING gist (geom);

-- 流域の面積を計算
UPDATE watershed_by_pref
SET area = st_area(st_geogfromwkb(geom), TRUE);


-- 流域の割合を計算
DROP TABLE IF EXISTS watershed_ratio;
CREATE TABLE watershed_ratio
(
    pref_id    VARCHAR,
    pref       VARCHAR,
    rank       INTEGER,
    river_code VARCHAR,
    river      VARCHAR,
    area       DOUBLE PRECISION,
    ratio      DOUBLE PRECISION
);
INSERT INTO watershed_ratio (pref_id, rank, pref, river_code, river, area, ratio)
WITH shed_ratio AS (SELECT p.id,
                           p.name,
                           p.area,
                           v.area                AS river_area,
                           v.river_code,
                           v.river,
                           v.area / p.area * 100 AS ratio
                    FROM pref AS p
                             JOIN watershed_by_pref AS v ON p.name = v.pref
                    ORDER BY p.name, ratio DESC)
SELECT id,
       ROW_NUMBER() OVER (PARTITION BY id ORDER BY ratio DESC) AS rank,
       name,
       river_code,
       river,
       river_area,
       ratio
FROM shed_ratio;
```
