-- 流域メッシュにインデックスを作成
CREATE INDEX IF NOT EXISTS idx_watershed_mesh_geom
    ON watershed_mesh USING gist (geom);
CREATE INDEX IF NOT EXISTS idx_watershed_mesh_w07_002_004
    ON watershed_mesh (w07_002, w07_004);

-- 河川ごとに流域メッシュを集計
DROP TABLE IF EXISTS watershed;
CREATE TABLE watershed
(
    gid        SERIAL PRIMARY KEY,
    river_code VARCHAR,
    name       VARCHAR,
    geom       geometry
);

INSERT INTO watershed (river_code, name, geom)
SELECT w07_002                              AS river_code,
       w07_004                              AS name,
       ST_Union(st_buffer(geom, 0.0000001)) AS geom
FROM watershed_mesh
WHERE w07_002 IS NOT NULL
  AND w07_004 IS NOT NULL
  -- 結構エイヤなデータがあるので除外
  AND w07_004 <> 'unknown'
  AND w07_004 <> '名称不明'
  AND w07_004 <> '不明'
  AND w07_004 <> '川'
  AND w07_004 <> '明'
GROUP BY river_code, name
;
CREATE INDEX idx_watershed_geom
    ON watershed USING gist (geom);

-- 流域メッシュの外周だけを抽出
DROP TABLE IF EXISTS watershed_boundary;
CREATE TABLE watershed_boundary
(
    gid        SERIAL PRIMARY KEY,
    river_code VARCHAR,
    name       VARCHAR,
    geom       geometry
)
;
INSERT INTO watershed_boundary (river_code, name, geom)
SELECT river_code,
       name,
       st_makepolygon(st_exteriorring(st_geometryn(st_multi(geom), 1))) AS geom
FROM watershed;

CREATE INDEX idx_watershed_boundary_geom
    ON watershed_boundary USING gist (geom);

-- 他の流域に含まれる流域を削除
DELETE
FROM watershed_boundary a
    USING watershed_boundary b
WHERE ST_Contains(b.geom, a.geom)
  AND a.gid <> b.gid;

-- 行政界ポリゴンにインデックス
CREATE INDEX idx_town_geom
    ON town USING gist (geom);

-- 都道府県ポリゴン
DROP TABLE IF EXISTS pref;
CREATE TABLE pref
(
    gid  SERIAL PRIMARY KEY,
    id   VARCHAR,
    name VARCHAR,
    area DOUBLE PRECISION,
    geom geometry
);

-- 都道府県ポリゴンを作成
INSERT INTO pref(name, geom)
SELECT n03_001, st_union(st_buffer(geom, 0.0000001)) AS geom
FROM town
GROUP BY n03_001;
CREATE INDEX idx_pref_geom
    ON pref USING gist (geom);

-- 都道府県ポリゴンの面積を計算
UPDATE pref
SET area = st_area(st_geogfromwkb(geom), TRUE);

-- 都道府県コードを設定
UPDATE pref p
SET id = (SELECT SUBSTR(n03_007, 1, 2)
          FROM town g
          WHERE p.name = g.n03_001
          LIMIT 1);


-- 都道府県ごとに流域を分割
DROP TABLE IF EXISTS watershed_by_pref;
CREATE TABLE watershed_by_pref
(
    river_code VARCHAR,
    river      VARCHAR,
    pref       VARCHAR,
    area       DOUBLE PRECISION,
    geom       geometry
);
INSERT INTO watershed_by_pref (river_code, river, pref, geom)
SELECT a.river_code,
       a.name                          AS river,
       b.name                          AS pref,
       st_intersection(a.geom, b.geom) AS geom
FROM watershed_boundary a
         JOIN pref b
              ON st_intersects(a.geom, b.geom)
;
CREATE INDEX idx_watershed_by_pref_geom
    ON watershed_by_pref USING gist (geom);

-- 流域の面積を計算
UPDATE watershed_by_pref
SET area = st_area(st_geogfromwkb(geom), TRUE);


-- 流域の割合を計算
DROP TABLE IF EXISTS watershed_ratio;
CREATE TABLE watershed_ratio
(
    pref_id    VARCHAR,
    pref       VARCHAR,
    rank       INTEGER,
    river_code VARCHAR,
    river      VARCHAR,
    area       DOUBLE PRECISION,
    ratio      DOUBLE PRECISION
);
INSERT INTO watershed_ratio (pref_id, rank, pref, river_code, river, area, ratio)
WITH shed_ratio AS (SELECT p.id,
                           p.name,
                           p.area,
                           v.area                AS river_area,
                           v.river_code,
                           v.river,
                           v.area / p.area * 100 AS ratio
                    FROM pref AS p
                             JOIN watershed_by_pref AS v ON p.name = v.pref
                    ORDER BY p.name, ratio DESC)
SELECT id,
       ROW_NUMBER() OVER (PARTITION BY id ORDER BY ratio DESC) AS rank,
       name,
       river_code,
       river,
       river_area,
       ratio
FROM shed_ratio;

