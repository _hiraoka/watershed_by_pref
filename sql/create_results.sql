-- 都道府県ごとに上位3つの流域を出力
with river_1st as (
    SELECT pref_id,
           rank,
           pref,
           river,
           ROUND(ratio::NUMERIC, 2) AS percent
    FROM watershed_ratio
    WHERE rank = 1
), river_2nd as (
    SELECT pref_id,
           rank,
           pref,
           river,
           ROUND(ratio::NUMERIC, 2) AS percent
    FROM watershed_ratio
    WHERE rank = 2
), river_3rd as (
    SELECT pref_id,
           rank,
           pref,
           river,
           ROUND(ratio::NUMERIC, 2) AS percent
    FROM watershed_ratio
    WHERE rank = 3
)
SELECT river_1st.pref_id,
       river_1st.pref,
       river_1st.river AS river_1st,
       river_1st.percent AS percent_1st,
       river_2nd.river AS river_2nd,
       river_2nd.percent AS percent_2nd,
       river_3rd.river AS river_3rd,
       river_3rd.percent AS percent_3rd
FROM river_1st
         INNER JOIN river_2nd ON river_1st.pref_id = river_2nd.pref_id
         INNER JOIN river_3rd ON river_1st.pref_id = river_3rd.pref_id
ORDER BY river_1st.pref_id;
;

-- 都道府県ごとに流域の面積比が1%以上の流域を出力
SELECT pref_id,
       rank,
       pref,
       river,
       ROUND(ratio::NUMERIC, 2) AS percent
FROM watershed_ratio
WHERE ratio >= 1.0;

-- 都道府県ごとに上位3つの流域を抽出してJSONで出力
WITH valley_within_3
         AS (SELECT pref_id,
                    pref,
                    river,
                    ratio,
                    rank
             FROM watershed_ratio
             WHERE rank <= 3),
     valley_river_agg
         AS (SELECT pref_id,
                    pref,
                    JSON_AGG(
                            JSON_BUILD_OBJECT(
                                    'rank', rank,
                                    'river', river,
                                    'ratio', ratio
                            )
                    ) AS rivers
             FROM valley_within_3
             GROUP BY pref_id,
                      pref
             ORDER BY pref_id)
SELECT JSON_AGG(
               JSON_BUILD_OBJECT(
                       'id', pref_id,
                       'pref', pref,
                       'rivers', rivers
               )
       ) AS rivers
FROM valley_river_agg
;
