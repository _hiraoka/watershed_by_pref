#!/usr/bin/env bash

source config.sh

# 国土数値情報流域メッシュデータをインポートする
#bash data/watershed/import_watershed.sh | tee -a $PARSE_LOG

# 国土数値情報行政区域データをインポートする
bash data/town/import_town.sh | tee -a $PARSE_LOG

# 都道府県別の流域データを作成する
export PGPASSWORD=$PG_PASSWORD
psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -f sql/create_watershed_by_pref.sql | tee -a $PARSE_LOG

# 都道府県ごとに上位3つの流域を抽出してJSONで出力
psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -t > chart/watershed_by_pref.json <<EOF
WITH valley_within_3
         AS (SELECT pref_id,
                    pref,
                    river,
                    ratio,
                    rank
             FROM watershed_ratio
             WHERE rank <= 3),
     valley_river_agg
         AS (SELECT pref_id,
                    pref,
                    JSON_AGG(
                            JSON_BUILD_OBJECT(
                                    'rank', rank,
                                    'river', river,
                                    'ratio', ratio
                            )
                    ) AS rivers
             FROM valley_within_3
             GROUP BY pref_id,
                      pref
             ORDER BY pref_id
             )
SELECT JSON_AGG(
               JSON_BUILD_OBJECT(
                       'id', pref_id,
                       'pref', pref,
                       'rivers', rivers
               )
       ) AS rivers
FROM valley_river_agg
;
EOF