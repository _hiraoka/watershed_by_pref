#!/bin/bash

# 生成ファイルを削除するか 
CLEAR_ALL_TEMP_FILES=true

# DB設定
PG_DATABASE=watershed_by_pref
PG_USER=postgres
PG_HOST=localhost
PG_PORT=5432
PG_PASSWORD=4i$r2wRuvlfadr8h+swehaq-$atHuduw

# 水系メッシュデータテーブル
TABLE_WATERSHED=watershed_mesh
SRID_WATERSHED=4612
# 行政界データテーブル
TABLE_TOWN=town
SRID_TOWN=4612

# log
PARSE_LOG=parse.log
