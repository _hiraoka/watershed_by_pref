d3.json("watershed_by_pref.json")
    .then((d) => {
        createChart(d);
        createDownloadLink();
    });

function createChart(source) {
    // データの整形
    const data = source.map(d => {
        return {
            id: d.id,
            name: d.pref,
            river1: d.rivers[0].river,
            river2: d.rivers[1].river,
            river3: d.rivers[2].river,
            ratio1: d.rivers[0].ratio,
            ratio2: d.rivers[1].ratio,
            ratio3: d.rivers[2].ratio,
            ratioOthers: 100 - (d.rivers[0].ratio + d.rivers[1].ratio + d.rivers[2].ratio)
        }
    });

    // グラフのサイズを設定
    const margin = {top: 20, right: 20, bottom: 20, left: 60};
    const width = 300 - margin.left - margin.right;
    const height = 600 - margin.top - margin.bottom;

    // SVG要素を生成
    const svg = d3.select("body")
        .append("svg")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("viewBox", "0 0 500 600")
        .attr("preserveAspectRatio", "xMinYMin")
        .append("g")
        .attr("transform", `translate(${margin.left}, ${margin.top})`);

    // 表示するデータのキーを設定
    const typeKeys = ["name", "ratio1", "ratio2", "ratio3", "ratioOthers", "river1", "river2", "river3"];

    // データを1位の河川割合でソート
    data.sort((a, b) => b.ratio1 - a.ratio1);

    // データを積み上げる
    const stack = d3.stack()
        .keys(typeKeys)
        .order(d3.stackOrderNone)
        .offset(d3.stackOffsetNone)
    const stackedData = stack(data)

    // X座標
    const formater = d3.format(".1s")
    const xScale = d3.scaleLinear()
        .domain([0, 100])
        .range([0, width])
    svg.append('g')
        .attr("transform", `translate(0, ${height})`)
        .call(
            d3.axisBottom(xScale)
                .ticks(7)
                .tickSize(0)
                .tickPadding(6)
                .tickFormat(formater)
        )
        .call(d => d.select(".domain").remove());

    // Y座標
    const yScale = d3.scaleBand()
        .domain(data.map(d => d.name))
        .range([0, height])
        .padding(.1);
    svg.append('g')
        .call(
            d3.axisLeft(yScale).tickSize(0).tickPadding(8)
        );

    // グラフ色
    const color = d3.scaleOrdinal()
        .domain(typeKeys)
        .range(['#fff', '#cf544d', '#5ba063', '#5573b9', '#656769']);

    // グラフのグリッド
    const GridLine = function () {
        return d3.axisBottom().scale(xScale)
    };
    svg.append("g")
        .attr("class", "grid")
        .call(GridLine()
            .tickSize(height, 0, 0)
            .tickFormat("")
            .ticks(8)
        );

    // グラフの描画
    svg.append("g")
        .selectAll("g")
        .data(stackedData)
        .join("g")
        .attr("fill", d => color(d.key))
        .attr("key", d => d.key)
        .selectAll("rect")
        .data(d => d)
        .join("rect")
        .attr("x", d => xScale(d[0]))
        .attr("y", d => yScale(d.data.name))
        .attr("width", d => xScale(d[1]) - xScale(d[0]))
        .attr("height", yScale.bandwidth())

    // グラフのラベルフォーマット
    const pp = d3.precisionFixed(0.1);
    const percentFormat = d3.format("." + pp + "f");

    // 一位の河川ラベル
    const layerLabel1 = svg.append("g")
    const label1Defs = layerLabel1.append("defs")

    const label1Data = data.map(d => {
        return {
            ratio: 0,
            ratio1: d.ratio1,
            pref: d.name,
            river: d.river1,
            percentage: percentFormat(d.ratio1) + "%"
        }
    })

    // clipPathを定義
    label1Defs
        .selectAll("clipPath")
        .data(label1Data)
        .join("clipPath")
        .attr("id", d => d.pref + d.river)
        .append("rect")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref))
        .attr("width", d => xScale(d.ratio1) - xScale(d.ratio))
        .attr("height", yScale.bandwidth())

    // ラベルを描画
    layerLabel1
        .selectAll("g")
        .data(label1Data)
        .join("text")
        .text(d => d.river)
        .attr("class", "river-label")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref) + yScale.bandwidth() / 2)
        .attr("dx", 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "start")
        .attr("clip-path", d => `url(#${d.pref + d.river})`)
    /*
    .append("tspan")
    .attr("class", "percentage")
    .text(d => " " + d.percentage)
     */

    // 2位の河川ラベル
    const layerLabel2 = svg.append("g")
    const label2Defs = layerLabel2.append("defs")

    // ラベルのデータを整形
    const label2Data = data.map(d => {
        return {
            ratio: d.ratio1,
            ratio1: d.ratio2 + d.ratio1,
            pref: d.name,
            river: d.river2,
            percentage: percentFormat(d.ratio2) + "%"
        }
    })

    // clipPathを定義
    label2Defs
        .selectAll("clipPath")
        .data(label2Data)
        .join("clipPath")
        .attr("id", d => d.pref + d.river)
        .append("rect")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref))
        .attr("width", d => xScale(d.ratio1) - xScale(d.ratio))
        .attr("height", yScale.bandwidth())

    // ラベルを描画
    layerLabel2
        .selectAll("g")
        .data(label2Data)
        .join("text")
        .text(d => d.river)
        .attr("class", "river-label")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref) + yScale.bandwidth() / 2)
        .attr("dx", 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "start")
        .attr("clip-path", d => `url(#${d.pref + d.river})`)
    /*
    .append("tspan")
    .attr("class", "percentage")
    .text(d => " " + d.percentage)
     */

    // 3位の河川ラベル
    const layerLabel3 = svg.append("g")
    const label3Defs = layerLabel3.append("defs")

    // ラベルのデータを整形
    const label3Data = data.map(d => {
        return {
            ratio: d.ratio1 + d.ratio2,
            ratio1: d.ratio3 + d.ratio2 + d.ratio1,
            pref: d.name,
            river: d.river3,
            percentage: percentFormat(d.ratio3) + "%"
        }
    })

    // clipPathを定義
    label3Defs
        .selectAll("clipPath")
        .data(label3Data)
        .join("clipPath")
        .attr("id", d => d.pref + d.river)
        .append("rect")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref))
        .attr("width", d => xScale(d.ratio1) - xScale(d.ratio))
        .attr("height", yScale.bandwidth())

    // ラベルを描画
    layerLabel3
        .selectAll("g")
        .data(label3Data)
        .join("text")
        .text(d => d.river)
        .attr("class", "river-label")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref) + yScale.bandwidth() / 2)
        .attr("dx", 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "start")
        .attr("clip-path", d => `url(#${d.pref + d.river})`)
    /*
    .append("tspan")
    .attr("class", "percentage")
    .text(d => " " + d.percentage)
     */

    // その他の河川ラベル
    const layerLabel4 = svg.append("g")
    const label4Defs = layerLabel4.append("defs")

    // ラベルのデータを整形
    const label4Data = data.map(d => {
        return {
            ratio: d.ratio1 + d.ratio2 + d.ratio3,
            ratio1: 100,
            pref: d.name,
            river: "その他",
            percentage: percentFormat(d.ratioOthers) + "%"
        }
    })

    // clipPathを定義
    label4Defs
        .selectAll("clipPath")
        .data(label4Data)
        .join("clipPath")
        .attr("id", d => d.pref + d.river)
        .append("rect")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref))
        .attr("width", d => xScale(d.ratio1) - xScale(d.ratio))
        .attr("height", yScale.bandwidth())

    // ラベルを描画
    layerLabel4
        .selectAll("g")
        .data(label4Data)
        .join("text")
        .text(d => d.river)
        .attr("class", "river-label-others")
        .attr("x", d => xScale(d.ratio))
        .attr("y", d => yScale(d.pref) + yScale.bandwidth() / 2)
        .attr("dx", 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "start")
        .attr("clip-path", d => `url(#${d.pref + d.river})`)
    /*
    .append("tspan")
    .attr("class", "percentage")
    .text(d => " " + d.percentage)
     */
}

function createDownloadLink() {
    // styleをSVG内に移動
    const styleElement = document.getElementsByTagName('style')[0];
    const svgElement = document.getElementsByTagName('svg')[0];
    svgElement.appendChild(styleElement);

    // SVG要素を文字列に変換
    const svgString = new XMLSerializer().serializeToString(d3.select('svg').node());

    // Blobオブジェクトを生成
    const blob = new Blob([svgString], {type: 'image/svg+xml;charset=utf-8'});

    // 生成したBlobオブジェクトのURLを生成
    const url = URL.createObjectURL(blob);

    // ダウンロードリンクを生成
    const link = document.createElement('a');
    link.href = url;
    link.download = 'watershed_by_pref.svg';
    link.text = 'Download';

    document.getElementsByTagName('body')[0].appendChild(link);

    // ダウンロードリンクをクリックしてSVGファイルをダウンロード
    // link.click();
}
