#!/usr/bin/env bash

THIS_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $THIS_DIR

source ../../config.sh

# 国土数値情報流域メッシュデータをダウンロードする
wget -i source.list -P zip --no-clobber

# ダウンロードしたファイルを解凍する
ls zip/*.zip | xargs -I {} unzip {}
if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f zip/*.zip
fi

# 解凍したファイルを shape ディレクトリに移動する
mkdir -p shape
find ./ -name "*.shp" | xargs -I{} mv {} shape
find ./ -name "*.shx" | xargs -I{} mv {} shape
find ./ -name "*.dbf" | xargs -I{} mv {} shape
rm *.xml
rm -R W07*

# shape ディレクトリにあるファイルをSQLに変換する
mkdir -p sql
ls shape/*.shp | while read shp 
do
    basename=$(basename $shp .shp)
    shp2pgsql -s $SRID_WATERSHED -D -W cp932 -a $shp $TABLE_WATERSHED > sql/$basename.sql
done

if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f shape/*.shp
    rm -f shape/*.shx
    rm -f shape/*.dbf
fi

export PGPASSWORD=$PG_PASSWORD

# データベースがなければ作成する
has_db=$(psql -U $PG_USER -h $PG_HOST -p $PG_PORT -c "SELECT 1 FROM pg_database WHERE datname = '$PG_DATABASE'" | grep 1)
if [ -z "$has_db" ]; then
    psql -U $PG_USER -h $PG_HOST -p $PG_PORT -c "CREATE DATABASE $PG_DATABASE"
    psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -c "CREATE EXTENSION postgis"
fi

# 流域界テーブルを作成する
psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE <<EOF
DROP TABLE IF EXISTS $TABLE_WATERSHED;
CREATE TABLE IF NOT EXISTS $TABLE_WATERSHED
(
    gid     SERIAL PRIMARY KEY,
    w07_001 VARCHAR,
    w07_002 VARCHAR,
    w07_003 VARCHAR,
    w07_004 VARCHAR,
    w07_005 VARCHAR,
    w07_006 VARCHAR,
    geom    geometry(MultiPolygon, 4612)
);
EOF

ls sql/*.sql | xargs -I{} psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -f {}

if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f sql/*.sql
fi
