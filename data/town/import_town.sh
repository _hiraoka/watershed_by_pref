#!/usr/bin/env bash

THIS_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $THIS_DIR

TARGET_YEAR=2021
MERGED_SQL=$THIS_DIR/merged_all.sql

source ../../config.sh

# 国土数値情報行政区域データをダウンロードする
# https://nlftp.mlit.go.jp/ksj/gml/data/N03/N03-2021/N03-20210101_01_GML.zip
mkdir -p zip
for i in {1..47}
do
    pcode=$(printf "%02d" $i)
    wget -P zip --no-clobber https://nlftp.mlit.go.jp/ksj/gml/data/N03/N03-${TARGET_YEAR}/N03-${TARGET_YEAR}0101_${pcode}_GML.zip
done

# ダウンロードしたファイルを解凍する
ls zip/*.zip | xargs -I {} unzip {}

if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f zip/*.zip
fi

# 解凍したファイルを shape ディレクトリに移動する
mkdir -p shape
find ./ -name "*.shp" | xargs -I{} mv {} shape
find ./ -name "*.shx" | xargs -I{} mv {} shape
find ./ -name "*.dbf" | xargs -I{} mv {} shape
rm N03-* -Rf

# shape ディレクトリにあるファイルをSQLに変換する
rm -f $MERGED_SQL
ls shape/*.shp | xargs -I{} shp2pgsql -s $SRID_TOWN -D -W cp932 -a {} $TABLE_TOWN >> $MERGED_SQL

if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f shape/*.shp
    rm -f shape/*.shx
    rm -f shape/*.dbf
fi

export PGPASSWORD=$PG_PASSWORD

# データベースがなければ作成する
has_db=$(psql -U $PG_USER -h $PG_HOST -p $PG_PORT -c "SELECT 1 FROM pg_database WHERE datname = '$PG_DATABASE'" | grep 1)
if [ -z "$has_db" ]; then
    psql -U $PG_USER -h $PG_HOST -p $PG_PORT -c "CREATE DATABASE $PG_DATABASE"
    psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -c "CREATE EXTENSION postgis"
fi

# 行政界テーブルを作成する
psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE <<EOF
DROP TABLE IF EXISTS $TABLE_TOWN;
CREATE TABLE IF NOT EXISTS $TABLE_TOWN
(
    gid     SERIAL PRIMARY KEY,
    n03_001 VARCHAR,
    n03_002 VARCHAR,
    n03_003 VARCHAR,
    n03_004 VARCHAR,
    n03_007 VARCHAR,
    geom    geometry(MultiPolygon, 4612)
);
EOF
psql -U $PG_USER -h $PG_HOST -p $PG_PORT -d $PG_DATABASE -f $MERGED_SQL

if [ $CLEAR_ALL_TEMP_FILES = "true" ]; then
    rm -f $MERGED_SQL
fi